<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    ->in(__DIR__);

$config = new PhpCsFixer\Config();

return $config->setRules([
    '@Symfony' => true,
    '@PSR2' => true,
    'array_indentation' => true,
    'indentation_type' => true,
    'method_chaining_indentation' => true,
    'no_extra_blank_lines' => [
        'tokens' => ['parenthesis_brace_block'],
    ],
    'array_syntax' => [
        'syntax' => 'short',
    ],
])->setFinder($finder);
