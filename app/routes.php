<?php

declare(strict_types=1);

use App\Application\Actions\Disbursement\BankAccountInquiryAction;
use App\Application\Actions\Disbursement\CheckBalanceAction;
use App\Application\Actions\Disbursement\CheckDisbursementAction;
use App\Application\Actions\Disbursement\CheckDisbursementReferenceAction;
use App\Application\Actions\Disbursement\ExecuteDisbursementAction;
use App\Application\Actions\Disbursement\HandleNotificationAction;
use App\Application\Actions\Disbursement\ListBanksAction;
use App\Application\Actions\Payment\CheckPaymentAction;
use App\Application\Actions\Payment\CheckPaymentReferenceAction;
use App\Application\Actions\Payment\HandlePaymentStatusAction;
use App\Application\Actions\Payment\HandleVaInquiryAction;
use App\Application\Actions\Payment\HandleVaPaymentAction;
use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');

        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });

    $app->group('/disbursement', function (Group $group) {
        $group->post('/notification', HandleNotificationAction::class);

        $group->get('/inquiry/{bank_id}/{account_number}/{customer_name}', BankAccountInquiryAction::class);
        $group->get('/banks', ListBanksAction::class);
        $group->get('/execute/{account_number}/{amount}/{bank_id}/{customer_name}/{description}/{reference_id}', ExecuteDisbursementAction::class);
        $group->get('/reference/{reference_id}', CheckDisbursementReferenceAction::class);
        $group->get('/id/{disbursement_id}', CheckDisbursementAction::class);

        $group->get('/balance', CheckBalanceAction::class);
    });

    $app->group('/payment', function (Group $group) {
        $group->post('/webhook-payment-status', HandlePaymentStatusAction::class);
        $group->post('/webhook-va-inquiry', HandleVaInquiryAction::class);
        $group->post('/webhook-va-payment', HandleVaPaymentAction::class);

        $group->get('/reference/{reference_id}', CheckPaymentReferenceAction::class);
        $group->get('/id/{payment_id}', CheckPaymentAction::class);
    });

    $app->group('/raw', function (Group $group) {
        $group->group('/disbursement', function (Group $group) {
            $group->get('/inquiry/{bank_id}/{account_number}/{customer_name}', \App\Application\Actions\Raw\BankAccountInquiryAction::class);
            $group->get('/banks', \App\Application\Actions\Raw\ListBanksAction::class);
            $group->get('/execute/{account_number}/{amount}/{bank_id}/{customer_name}/{description}/{reference_id}', \App\Application\Actions\Raw\ExecuteDisbursementAction::class);
        });
    });
};
