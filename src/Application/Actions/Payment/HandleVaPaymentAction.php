<?php

namespace App\Application\Actions\Payment;

use Psr\Http\Message\ResponseInterface as Response;

class HandleVaPaymentAction extends PaymentAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $this->logger->info('VA payment:');
        $this->logger->info($this->request->getBody()->getContents());
        $this->logger->info(json_encode($this->request->getHeaders()));

        return $this->respondWithData(['reference_id' => '']);
    }
}
