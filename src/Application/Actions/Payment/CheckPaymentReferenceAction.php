<?php

namespace App\Application\Actions\Payment;

use Psr\Http\Message\ResponseInterface as Response;

class CheckPaymentReferenceAction extends PaymentAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $referenceId = $this->resolveArg('reference_id');
        $payment = $this->payment->checkPaymentByReferenceId($referenceId);

        return $this->respondWithData($payment);
    }
}
