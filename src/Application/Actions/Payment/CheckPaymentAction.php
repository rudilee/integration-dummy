<?php

namespace App\Application\Actions\Payment;

use Psr\Http\Message\ResponseInterface as Response;

class CheckPaymentAction extends PaymentAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $paymentId = $this->resolveArg('payment_id');
        $payment = $this->payment->checkPaymentByPaymentId($paymentId);

        return $this->respondWithData($payment);
    }
}
