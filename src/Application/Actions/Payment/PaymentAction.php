<?php

namespace App\Application\Actions\Payment;

use App\Application\Actions\Action;
use App\Application\Actions\OdeoApiSession;
use OdeoApi\Services\PaymentGateway;
use Psr\Log\LoggerInterface;

abstract class PaymentAction extends Action
{
    use OdeoApiSession;

    protected $payment;

    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger);

        $this->payment = new PaymentGateway();
        $this->startOdeoSession($this->payment);
    }
}
