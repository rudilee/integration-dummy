<?php

namespace App\Application\Actions\Payment;

use Psr\Http\Message\ResponseInterface as Response;

class HandleVaInquiryAction extends PaymentAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $this->logger->info('VA inquiry:');
        $this->logger->info($this->request->getBody()->getContents());
        $this->logger->info(json_encode($this->request->getHeaders()));

        return $this->respondWithData([
            'customer_name' => 'Agus Budi Cahyo',
            'item_name' => 'Panci Indomie',
            'amount' => '150000',
            'display_text' => 'Bayar Panci Indomie',
        ]);
    }
}
