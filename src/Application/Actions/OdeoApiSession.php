<?php

namespace App\Application\Actions;

use App\Application\Settings\EnvSettings;
use OdeoApi\OdeoApi;

trait OdeoApiSession
{
    public function startOdeoSession(OdeoApi $odeoApi): void
    {
        EnvSettings::init();

        $odeoApi->setBaseUrl($_ENV['BASE_URL']);
        $odeoApi->setCredentials($_ENV['CLIENT_ID'], $_ENV['SECRET_KEY'], $_ENV['SIGNING_KEY']);

        session_start(['cookie_lifetime' => 600]);
        if (!isset($_SESSION['access_token'])) {
            $_SESSION['access_token'] = $odeoApi->refreshAccessToken()['access_token'];
        }

        $odeoApi->setAccessToken($_SESSION['access_token']);
    }
}
