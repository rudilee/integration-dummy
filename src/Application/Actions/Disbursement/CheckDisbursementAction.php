<?php

namespace App\Application\Actions\Disbursement;

use Psr\Http\Message\ResponseInterface as Response;

class CheckDisbursementAction extends DisbursementAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $disbursementId = $this->resolveArg('disbursement_id');
        $disbursement = $this->disbursement->checkDisbursementByDisbursementId($disbursementId);

        return $this->respondWithData($disbursement);
    }
}
