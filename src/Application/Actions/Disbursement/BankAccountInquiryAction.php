<?php

namespace App\Application\Actions\Disbursement;

use Psr\Http\Message\ResponseInterface as Response;

class BankAccountInquiryAction extends DisbursementAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $bankId = $this->resolveArg('bank_id');
        $accountNumber = $this->resolveArg('account_number');
        $customerName = $this->resolveArg('customer_name');

        $accountHolderName = $this->disbursement->bankAccountInquiry($accountNumber, $bankId, $customerName);

        return $this->respondWithData($accountHolderName);
    }
}
