<?php

namespace App\Application\Actions\Disbursement;

use Psr\Http\Message\ResponseInterface as Response;

class ListBanksAction extends DisbursementAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $banks = $this->disbursement->bankList();

        return $this->respondWithData($banks);
    }
}
