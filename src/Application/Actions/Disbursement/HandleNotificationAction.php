<?php

namespace App\Application\Actions\Disbursement;

use App\Application\Actions\ActionPayload;
use Psr\Http\Message\ResponseInterface as Response;

class HandleNotificationAction extends DisbursementAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $this->logger->info('Disbursement notification:');
        $this->logger->info($this->request->getBody()->getContents());
        $this->logger->info(json_encode($this->request->getHeaders()));

        return $this->respond(new ActionPayload());
    }
}
