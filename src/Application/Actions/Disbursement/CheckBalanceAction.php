<?php

namespace App\Application\Actions\Disbursement;

use Psr\Http\Message\ResponseInterface as Response;

class CheckBalanceAction extends DisbursementAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $balance = $this->disbursement->checkBalance();

        return $this->respondWithData($balance);
    }
}
