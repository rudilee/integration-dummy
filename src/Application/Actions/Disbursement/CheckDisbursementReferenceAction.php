<?php

namespace App\Application\Actions\Disbursement;

use Psr\Http\Message\ResponseInterface as Response;

class CheckDisbursementReferenceAction extends DisbursementAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $referenceId = $this->resolveArg('reference_id');
        $disbursement = $this->disbursement->checkDisbursementByReferenceId($referenceId);

        return $this->respondWithData($disbursement);
    }
}
