<?php

namespace App\Application\Actions\Disbursement;

use App\Application\Actions\Action;
use App\Application\Actions\OdeoApiSession;
use OdeoApi\Services\Disbursement;
use Psr\Log\LoggerInterface;

abstract class DisbursementAction extends Action
{
    use OdeoApiSession;

    protected $disbursement;

    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger);

        $this->disbursement = new Disbursement();
        $this->startOdeoSession($this->disbursement);
    }
}
