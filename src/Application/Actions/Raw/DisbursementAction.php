<?php

namespace App\Application\Actions\Raw;

use App\Application\Actions\Action;
use App\Application\Settings\EnvSettings;
use Psr\Log\LoggerInterface;

abstract class DisbursementAction extends Action
{
    protected $odeoApi;

    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger);
        EnvSettings::init();

        $this->odeoApi = new OdeoApi(
            $_ENV['BASE_URL'],
            $_ENV['CLIENT_ID'],
            $_ENV['SECRET_KEY'],
            $_ENV['SIGNING_KEY']
        );

        session_start(['cookie_lifetime' => 600]);
        if (!isset($_SESSION['access_token'])) {
            $_SESSION['access_token'] = $this->odeoApi->requestOAuth2AccessToken();
        }

        $this->odeoApi->setAccessToken($_SESSION['access_token']);
    }
}
