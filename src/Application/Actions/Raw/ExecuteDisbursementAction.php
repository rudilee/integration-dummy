<?php

namespace App\Application\Actions\Raw;

use Psr\Http\Message\ResponseInterface as Response;

class ExecuteDisbursementAction extends DisbursementAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        $accountNumber = $this->resolveArg('account_number');
        $amount = $this->resolveArg('amount');
        $bankId = (int) $this->resolveArg('bank_id');
        $customerName = $this->resolveArg('customer_name');
        $description = $this->resolveArg('description');
        $referenceId = $this->resolveArg('reference_id');

        $result = $this->odeoApi->createDisbursement($accountNumber, $amount, $bankId, $customerName, $referenceId, $description);

        return $this->respondWithData($result);
    }
}
