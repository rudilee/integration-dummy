<?php

namespace App\Application\Actions\Raw;

use Psr\Http\Message\ResponseInterface as Response;

class ListBanksAction extends DisbursementAction
{
    /**
     * {@inheritDoc}
     */
    protected function action(): Response
    {
        return $this->respondWithData($this->odeoApi->listBanks());
    }
}
