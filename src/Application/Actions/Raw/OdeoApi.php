<?php

namespace App\Application\Actions\Raw;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class OdeoApi
{
    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';

    protected $baseUrl;
    protected $clientId;
    protected $clientSecret;
    protected $signingKey;
    protected $accessToken = '';

    public function __construct($baseUrl, $clientId, $clientSecret, $signingKey)
    {
        $this->baseUrl = $baseUrl;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->signingKey = $signingKey;
    }

    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public static function generateSignature(
        string $signingKey,
        string $httpMethod,
        string $path,
        string $queryString,
        string $accessToken,
        int $timestamp,
        $requestBody = ''
    ): string {
        $algo = 'sha256';
        $bodyHash = base64_encode(hash(
            $algo,
            is_array($requestBody) ? json_encode($requestBody) : $requestBody,
            true
        ));
        $stringToSign = "$httpMethod:$path:$queryString:$accessToken:$timestamp:$bodyHash";

        return base64_encode(hash_hmac($algo, $stringToSign, $signingKey, true));
    }

    protected function createAuthRequest(
        string $httpMethod,
        string $path,
        $queryString = '',
        $requestBody = ''
    ): ResponseInterface {
        $timestamp = time();

        $headers = [
            'Content-Type' => 'application/json',
            'Accept-Language' => 'en',
        ];

        if (!empty($this->accessToken)) {
            $headers['X-Odeo-Timestamp'] = $timestamp;
            $headers['X-Odeo-Signature'] = self::generateSignature(
                $this->signingKey,
                $httpMethod,
                "/$path",
                $queryString,
                $this->accessToken,
                $timestamp,
                $requestBody
            );

            $headers['Authorization'] = "bearer $this->accessToken";
        }

        return (new Client())->request(
            $httpMethod,
            "$this->baseUrl/$path",
            [
                'headers' => $headers,
                'body' => is_array($requestBody) ? json_encode($requestBody) : $requestBody,
            ]
        );
    }

    public function requestOAuth2AccessToken(): string
    {
        $request = $this->createAuthRequest(
            self::HTTP_POST,
            'oauth2/token',
            '',
            [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'scope' => '',
            ]
        );
        $contents = json_decode($request->getBody()->getContents());

        return $contents->access_token;
    }

    public function bankAccountInquiry(
        string $accountNumber,
        int $bankId,
        string $customerName,
        bool $withValidation = false
    ): array {
        $request = $this->createAuthRequest(
            self::HTTP_POST,
            'dg/v1/bank-account-inquiry',
            '',
            [
                'account_number' => $accountNumber,
                'bank_id' => $bankId,
                'customer_name' => $customerName,
                'with_validation' => $withValidation,
            ]
        );

        return json_decode($request->getBody()->getContents(), true);
    }

    public function listBanks(): array
    {
        $request = $this->createAuthRequest(
            self::HTTP_GET,
            'dg/v1/banks'
        );
        $contents = json_decode($request->getBody()->getContents());

        return $contents->banks;
    }

    public function createDisbursement(
        string $accountNumber,
        string $amount,
        int $bankId,
        string $customerName,
        string $referenceId,
        string $description = ''
    ): array {
        $request = $this->createAuthRequest(
            self::HTTP_POST,
            'dg/v1/disbursements',
            '',
            [
                'account_number' => $accountNumber,
                'amount' => $amount,
                'bank_id' => $bankId,
                'customer_name' => $customerName,
                'reference_id' => $referenceId,
                'description' => $description,
            ]
        );

        return json_decode($request->getBody()->getContents(), true);
    }

    public function getDisbursementByReferenceId(string $referenceId): array
    {
        $request = $this->createAuthRequest(
            self::HTTP_GET,
            "dg/v1/disbursement/reference-id/$referenceId"
        );

        return json_decode($request->getBody()->getContents());
    }

    public function getDisbursement(int $disbursementId): array
    {
        $request = $this->createAuthRequest(
            self::HTTP_GET,
            "dg/v1/disbursement/$disbursementId"
        );

        return json_decode($request->getBody()->getContents(), true);
    }

    public function getPaymentByReferenceId(string $referenceId): array
    {
        $request = $this->createAuthRequest(
            self::HTTP_GET,
            "pg/v1/payment/reference-id/$referenceId"
        );

        return json_decode($request->getBody()->getContents());
    }

    public function getPayment(int $paymentId): array
    {
        $request = $this->createAuthRequest(
            self::HTTP_GET,
            "pg/v1/payment/$paymentId"
        );

        return json_decode($request->getBody()->getContents(), true);
    }
}
