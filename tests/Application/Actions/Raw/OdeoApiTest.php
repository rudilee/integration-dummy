<?php

namespace Tests\Application\Actions\Raw;

use App\Application\Actions\Raw\OdeoApi;
use OdeoApi\Services\Disbursement;
use PHPUnit\Framework\TestCase;

class OdeoApiTest extends TestCase
{
    public function testGenerateSignature()
    {
        $expected = 'JhWklt2rgpFYreQ88U5StXNdkJFfmOlumstoR1J+x+k=';
        $actual = OdeoApi::generateSignature(
            'Key',
            OdeoApi::HTTP_GET,
            '/path',
            '',
            'token',
            123456
        );

        $this->assertEquals($expected, $actual);
    }

    public function testValidateSignatureWithOdeoSdk()
    {
        $signingKey = 'Key';
        $path = '/path';
        $accessToken = 'token';
        $timestamp = 123456;
        $requestBody = ['test' => 123];

        $signatureToCompare = OdeoApi::generateSignature(
            $signingKey,
            OdeoApi::HTTP_GET,
            $path,
            '',
            $accessToken,
            $timestamp,
            $requestBody
        );

        $disbursement = new Disbursement();
        $disbursement->setCredentials('', '', $signingKey);

        $this->assertTrue($disbursement->isValidSignature(
            $signatureToCompare,
            OdeoApi::HTTP_GET,
            $path,
            $timestamp,
            $requestBody,
            $accessToken
        ));
    }
}
